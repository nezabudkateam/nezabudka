﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NeZabudka.Authentification;
using NeZabudka.Data;
using NeZabudka.Web.Client;
using NeZabudka.Web.Client.Infrastructure.IoC;
using Owin;
using StructureMap;

[assembly: OwinStartup(typeof(Startup))]

namespace NeZabudka.Web.Client
{
	public partial class Startup
	{
		public static Container Container;

		public void Configuration(IAppBuilder app)
		{
			DataLayerConfiguration.Configure();
			ConfigureIoC();
			app.UseNeZabudkaAuthentication();
			ConfigureMvc(app);
		}

		protected virtual void ConfigureIoC()
		{
			Container = new Container();
			Container.Configure(x => x.AddRegistry(new AppRegistry()));
		}

		private void ConfigureMvc(IAppBuilder app)
		{
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters, Container);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory(Container));

			var configuration = new HttpConfiguration();
			configuration.Services.Replace(typeof(IHttpControllerActivator), new StructureMapHttpControllerActivator(Container));
			configuration.Formatters.Remove(configuration.Formatters.XmlFormatter);
			configuration.Formatters.JsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
			configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			configuration.Filters.Add(new HostAuthenticationFilter(AuthentificationTypes.User));
			configuration.MapHttpAttributeRoutes();
			app.UseWebApi(configuration);			
		}
	}
}