﻿using System.Web.Mvc;

namespace NeZabudka.Web.Client.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index() {
		    ViewBag.IsAuthenticated = User.Identity.IsAuthenticated;
			return View();
		}
	}
}