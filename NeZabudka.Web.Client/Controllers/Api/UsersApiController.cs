﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using NeZabudka.Services;
using NeZabudka.Web.Client.Models;

namespace NeZabudka.Web.Client.Controllers.Api
{
	[RoutePrefix("api/users")]
	public class UsersApiController : ApiController
	{
		private readonly IUserService userService;

		public UsersApiController(IUserService userService)
		{
			this.userService = userService;
		}

		[HttpGet, Route("{*keyword}")]
		public async Task<List<UserModel>> Search(string keyword)
		{
			var users = await userService.FindUsersByKeyword(keyword, 10);
			return users.Select(UserModel.MapFrom).ToList();
		}
	}
}
