﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using MongoDB.Bson;
using NeZabudka.Authentification;
using NeZabudka.Domain.Identity;
using NeZabudka.Services;
using NeZabudka.Web.Client.Models;

namespace NeZabudka.Web.Client.Controllers.Api
{
	[RoutePrefix("api/login")]
	public class LoginApiController : ApiController
	{
		private readonly IUserService userService;

		public LoginApiController(IUserService userService)
		{
			this.userService = userService;
		}

		private void LoginAs(User user)
		{
			var data = new AuthData
			{
				Id = user.Id.ToString(),
				Email = user.Email,
				Name = user.Name
			};
			AuthManager.Login(data, true);
		}

		[HttpGet, Route("")]
		public async Task LoginForDebug()
		{
			var user = await userService.GetUser(ObjectId.Parse("573713665c2b66189cfef077"));
			LoginAs(user);
		}

		[HttpPost, Route("")]
		public async Task Login(LoginModel model)
		{
			var user = await userService.GetUserByEmail(model.Email);
			if (user == null)
			{
				throw new HttpResponseException(HttpStatusCode.NotFound);
			}
			if (user.Password != model.Password)
			{
				throw new HttpResponseException(HttpStatusCode.BadRequest);
			}
			LoginAs(user);
		}

		[HttpPost, Route("register")]
		public async Task Register(RegisterModel model)
		{
			var user = await userService.GetUserByEmail(model.Email);
			if (user != null)
			{
				throw new HttpResponseException(HttpStatusCode.Conflict);
			}
			user = await userService.Register(email: model.Email, password: model.Password, name: model.Name);
			LoginAs(user);
		}

		[HttpDelete, Route("")]
		public void Logout()
		{
			AuthManager.Logout();
		}
	}
}
