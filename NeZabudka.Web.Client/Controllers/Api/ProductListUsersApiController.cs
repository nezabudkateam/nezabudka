﻿using System.Threading.Tasks;
using System.Web.Http;
using MongoDB.Bson;
using NeZabudka.Services;

namespace NeZabudka.Web.Client.Controllers.Api
{
	[RoutePrefix("api/product-lists/{listId}/users/{userId}")]
	public class ProductListUsersApiController : BaseApiController
	{
		private readonly IProductListService productListService;

		public ProductListUsersApiController(IProductListService productListService)
		{
			this.productListService = productListService;
		}

		[HttpPut, Route("")]
		public Task Add(string listId, string userId)
		{
			return productListService.AddUser(ObjectId.Parse(listId), ObjectId.Parse(userId));
		}

		[HttpDelete, Route("")]
		public Task Delete(string listId, string userId)
		{
			return productListService.DeleteUser(ObjectId.Parse(listId), ObjectId.Parse(userId));
		}
	}
}
