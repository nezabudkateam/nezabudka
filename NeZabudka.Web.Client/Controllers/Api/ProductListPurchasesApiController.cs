﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using MongoDB.Bson;
using NeZabudka.Services;

namespace NeZabudka.Web.Client.Controllers.Api
{
	[RoutePrefix("api/product-lists/{listId}/purchases")]
	public class ProductListPurchasesApiController : BaseApiController
	{
		private readonly IProductService productService;
		private readonly IProductListService productListService;

		public ProductListPurchasesApiController(
			IProductService productService,
			IProductListService productListService)
		{
			this.productService = productService;
			this.productListService = productListService;
		}

		[HttpDelete, Route("{purchaseId}")]
		public Task Restore(string listId, string purchaseId)
		{
			return productListService.RestorePurchase(ObjectId.Parse(listId), ObjectId.Parse(purchaseId));
		}
	}
}
