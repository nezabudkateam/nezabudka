﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using MongoDB.Bson;
using NeZabudka.Domain.Products;
using NeZabudka.Services;
using NeZabudka.Web.Client.Models;

namespace NeZabudka.Web.Client.Controllers.Api
{
	[RoutePrefix("api/product-lists")]
	public class ProductListsApiController : BaseApiController
	{
		private readonly IUserService userService;
		private readonly IProductService productService;
		private readonly IProductListService productListService;
		private readonly IProductSuggestionService productSuggestionService;

		public ProductListsApiController(
			IUserService userService,
			IProductService productService,
			IProductListService productListService,
			IProductSuggestionService productSuggestionService)
		{
			this.userService = userService;
			this.productService = productService;
			this.productListService = productListService;
			this.productSuggestionService = productSuggestionService;
		}

		private async Task<UserModel> MapUser(ObjectId id)
		{
			var user = await userService.GetUser(id);
			return UserModel.MapFrom(user);
		}

		private async Task<ProductListItemModel> MapItem(ProductListItem item)
		{
			var product = await productService.GetProduct(item.ProductId);
			return new ProductListItemModel
			{
				Product = ProductModel.MapFrom(product),
				CreatorId = item.CreatorId.ToString(),
				CreationTime = item.CreationTime
			};
		}

		private async Task<ProductModel> MapSuggestion(ProductSuggestion suggestion)
		{
			return ProductModel.MapFrom(await productService.GetProduct(ObjectId.Parse(suggestion.ProductId)));
		}

		private async Task<ProductListPurchaseModel> MapPurchase(ProductPurchase purchase)
		{
			var product = await productService.GetProduct(purchase.Info.ProductId);
			return new ProductListPurchaseModel
			{
				Id = purchase.Id.ToString(),
				Product = ProductModel.MapFrom(product),
				Date = purchase.Info.Date,
				User = await MapUser(purchase.UserId)
			};
		}

		private async Task<ProductListModel> Map(ProductList productList)
		{
			return new ProductListModel
			{
				Id = productList.Id.ToString(),
				Name = productList.Name,
				CreatorId = productList.CreatorId.ToString(),
				Users = await Task.WhenAll(productList.UserIds.Select(MapUser)),
				Items = await Task.WhenAll(productList.Items.Select(MapItem)),
				Suggestions = new ProductModel[0],
				Completed = new ProductListPurchaseModel[0]
			};
		}

		private async Task<ProductModel[]> GetSuggestionModels(ProductList list)
		{
			try
			{
				var suggestions = await productSuggestionService.GetSuggestions(list);
				return await Task.WhenAll(suggestions.Select(MapSuggestion));
			}
			catch (Exception)
			{
				return new ProductModel[0];
			}
		}

		private async Task<ProductListPurchaseModel[]> GetPurchaseModels(ProductList list)
		{
			var now = DateTime.UtcNow;
			var purchases = await productListService.GetPurchases(list.Id);
			return await Task.WhenAll(purchases.Where(p => p.Info.Date.AddDays(1) > now).OrderByDescending(p => p.Info.Date).Select(MapPurchase));
		}

		[HttpGet, Route("")]
		public async Task<ProductListModel[]> List()
		{
			var lists = await productListService.GetByUser(CurrentUserId);
			return await Task.WhenAll(lists.Select(Map));
		}

		[HttpGet, Route("{id}")]
		public async Task<ProductListModel> Get(string id)
		{
			var list = await productListService.GetList(ObjectId.Parse(id));
			var model = await Map(list);
			model.Suggestions = await GetSuggestionModels(list);
			model.Completed = await GetPurchaseModels(list);
			return model;
		}

		[HttpGet, Route("{id}/suggestions")]
		public async Task<ProductModel[]> GetSuggestions(string id)
		{
			var list = await productListService.GetList(ObjectId.Parse(id));
			return await GetSuggestionModels(list);
		}

		[HttpPost, Route("")]
		public async Task<ProductListModel> Create()
		{
			return await Map(await productListService.Create(CurrentUserId));
		}

		[HttpPost, Route("{id}")]
		public Task Rename(string id, [FromBody]string newName)
		{
			return productListService.Rename(ObjectId.Parse(id), newName);
		}

		[HttpGet, Route("{id}/search")]
		public async Task<List<ProductModel>> Search(string id, string keyword)
		{
			var products = await productService.GetProductsByKeyword(keyword, 10, ObjectId.Parse(id));
			return products.Select(ProductModel.MapFrom).ToList();
		}

		[HttpDelete, Route("{id}")]
		public async Task Delete(string id)
		{
			var list = await productListService.GetList(ObjectId.Parse(id));
			var userId = CurrentUserId;
			if (list.CreatorId == userId)
			{
				await productListService.Delete(list.Id);
			}
			else
			{
				await productListService.DeleteUser(list.Id, userId);
			}
		}
	}
}
