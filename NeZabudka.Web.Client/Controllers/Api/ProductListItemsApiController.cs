﻿using System.Threading.Tasks;
using System.Web.Http;
using MongoDB.Bson;
using NeZabudka.Services;

namespace NeZabudka.Web.Client.Controllers.Api
{
	[RoutePrefix("api/product-lists/{listId}/items")]
	public class ProductListItemsApiController : BaseApiController
	{
		private readonly IProductService productService;
		private readonly IProductListService productListService;

		public ProductListItemsApiController(
			IProductService productService,
			IProductListService productListService)
		{
			this.productService = productService;
			this.productListService = productListService;
		}

		[HttpPut, Route("{productId}")]
		public Task Add(string listId, string productId)
		{
			return productListService.AddItem(ObjectId.Parse(listId), ObjectId.Parse(productId), CurrentUserId);
		}

		[HttpPut, Route("")]
		public async Task AddNew(string listId, string productName)
		{
			var id = ObjectId.Parse(listId);
			var product = await productService.AddPrivateProduct(productName, id);
			await productListService.AddItem(id, product.Id, CurrentUserId);
		}

		[HttpPost, Route("{productId}")]
		public Task Complete(string listId, string productId)
		{			
			return productListService.DeleteItem(ObjectId.Parse(listId), ObjectId.Parse(productId), completed: true, userId: CurrentUserId);
		}

		[HttpDelete, Route("{productId}")]
		public Task Delete(string listId, string productId)
		{
			return productListService.DeleteItem(ObjectId.Parse(listId), ObjectId.Parse(productId), completed: false, userId: CurrentUserId);
		}
	}
}
