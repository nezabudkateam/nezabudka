﻿using System.Web.Http;
using MongoDB.Bson;
using NeZabudka.Authentification;

namespace NeZabudka.Web.Client.Controllers.Api
{
	[Authorize]
	public abstract class BaseApiController : ApiController
	{
		public AuthData CurrentAuthData => AuthManager.GetCurrentData();
		public ObjectId CurrentUserId => ObjectId.Parse(CurrentAuthData.Id);
	}
}
