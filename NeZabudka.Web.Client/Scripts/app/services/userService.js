﻿(function () {
    "use strict";
    var api = angular.module("app");

    api.service("userService", [
        "apiServiceBase", function (apiServiceBase) {
            function getUrlBase() {
                return "/api/users";
            }

            //            this.list = function() {
            //                return apiServiceBase.get(getUrlBase());
            //            }
            //
            //            this.get = function (id) {
            //                return apiServiceBase.get(getUrlBase() + '/' + id);
            //            }

            this.search = function (term) {
                return apiServiceBase.get(getUrlBase() + '/' + term);
            }
        }
    ]);
})();