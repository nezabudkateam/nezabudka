﻿(function() {
    var api = angular.module("api", ["common", "ui.router"]);

    var serviceId = "apiServiceBase";
    api.service(serviceId, [
        "$http", "$rootScope","$state", function ($http, $rootScope, $state) {
            $rootScope.$watch(
                function () { return $http.pendingRequests.length; },
                function (currentRequests) {
                    //console.log('requests watcher ' + currentRequests);
                    $rootScope.loadingProgress = currentRequests > 0;
                }
            );

            this.get = function () {
                return $http.get.apply(this, arguments).error(errorHandler);
            }

            this.post = function () {
                return $http.post.apply(this, arguments).error(errorHandler);
            }

            this.put = function () {
                return $http.put.apply(this, arguments).error(errorHandler);
            }

            this.delete = function () {
                return $http.delete.apply(this, arguments).error(errorHandler);
            }

            function errorHandler(err, status) {
                if (status === 401) {
                    $state.go("login");
                    return;
                }

                console.log(arguments);
            }
        }
    ]);

    
})();