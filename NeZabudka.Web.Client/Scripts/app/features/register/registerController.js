﻿(function (app) {
    app.controller('RegisterController', [
        '$scope', '$state', 'common','authService',
        function ($scope, $state,common, authService) {
            var vm = this;

            vm.name = "";
            vm.password = "";
            vm.email = "";

            vm.login = function() {
                var model = {
                    email: vm.email,
                    password: vm.password,
                    name: vm.name
                }

                authService.register(model).success(function () {
                    $state.go("lists.list");
                });
            }

            common.$rootScope.title = "Регистрация";
        }
    ]);
})(app);

