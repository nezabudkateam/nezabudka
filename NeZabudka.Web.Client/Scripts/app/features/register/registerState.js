﻿(function(app) {
    app.config(function($stateProvider, $urlRouterProvider) {

        //
        // Now set up the states
        $stateProvider
            .state('register', {
                parent: 'auth',
                url: "/register",
                templateUrl: 'scripts/app/features/register/_register.html',
                controller: 'RegisterController',
                controllerAs: 'vm'
            });
    });
})(app);