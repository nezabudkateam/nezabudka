﻿(function (app) {
    app.controller('HeaderController', [
        '$scope','$state','authService',
        function ($scope, $state,authService) {
            var vm = this;

            vm.settings = function() {
            }

            vm.logout = function() {
                authService.logout().success(function() {
                    $state.go("login");
                });
            }
        }
    ]);
})(app);

