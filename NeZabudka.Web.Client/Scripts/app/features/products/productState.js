﻿(function(app) {
    app.config(function($stateProvider, $urlRouterProvider) {

        //
        // Now set up the states
        $stateProvider
            .state('products', {
                parent: 'lists',
                abstract: true,
                template: "<div ui-view flex layout='column'></div>"
            })
            .state('products.list', {
                url: "/:listId/products",
                templateUrl: "/scripts/app/features/products/_list.html",
                controller: "ProductsListController",
                controllerAs: "vm"
            });
    });
})(app);