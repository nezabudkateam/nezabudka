﻿(function () {
    "use strict";
    var api = angular.module("api");

    api.service("productListItemsService", [
        "apiServiceBase", function (apiServiceBase) {
            function getUrlBase(listId, productId) {
                return "api/product-lists/"+listId+"/items" ;
            }

//            this.list = function() {
//                return apiServiceBase.get(getUrlBase());
//            }
//
//            this.get = function (id) {
//                return apiServiceBase.get(getUrlBase() + '/' + id);
//            }
            this.add = function (listId, productId) {
                return apiServiceBase.put(getUrlBase(listId) + "/"+ productId);
            }

            this.complete = function (listId, productId) {
                return apiServiceBase.post(getUrlBase(listId) + "/" + productId);
            }

            this.delete = function (listId, productId) {
                return apiServiceBase.delete(getUrlBase(listId) + "/" + productId);
            }

            this.restore = function (listId, purchaseId) {
                return apiServiceBase.delete("api/product-lists/" + listId + "/purchases/" + purchaseId);
            }

            this.addNewProduct = function (listId, productName) {
                return apiServiceBase.put(getUrlBase(listId) + '?productName='+ productName);
            }
        }
    ]);
})();