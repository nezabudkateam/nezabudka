﻿(function (app) {
    app.controller('ProductsListController', [
        '$scope', '$state', 'common','spinnerService', 'productListService', 'productListItemsService', 'productService',
        function ($scope, $state, common, spinnerService, productListService, productListItemsService, productService) {
            var vm = this;

            var $timeout = common.$timeout;
            var $interval = common.$interval;
            var $q = common.$q;

            var listId = $state.params.listId;

            vm.listId = listId;

            vm.products = [];

            vm.isEmpty = function () {
                return !vm.products || vm.products.length == 0;
            }

            vm.spinner = new spinnerService();

            vm.touch = function (item) {
                if (item.removing || item.compliting) {
                    item.removing = item.compliting = false;
                    $interval.cancel(item.deleteIntervalPromise);

                    delete item.deleteIntervalPromise;
                } else {
                    item.deleteWaiting = 0;
                    item.compliting = true;

                    item.deleteIntervalPromise = $interval(function () {
                        item.deleteWaiting += 4;

                        if (item.deleteWaiting >= 125) {
                            var deletePromise = productListItemsService.complete(vm.listId, item.product.id).success(function () {
                                //vm.products.remove(item);
                                reloadItems();
                            });

                            vm.spinner.registerPromise(deletePromise);
                            
                            $interval.cancel(item.deleteIntervalPromise);
                        }
                 
                    }, 50, 0, true);
                }
            }

            vm.deleteTouch = function (item) {
                if (item.removing || item.compliting) {
                    item.removing = item.compliting = false;
                    $interval.cancel(item.deleteIntervalPromise);

                    delete item.deleteIntervalPromise;
                } else {
                    item.deleteWaiting = 0;
                    item.removing = true;

                    item.deleteIntervalPromise = $interval(function () {
                        item.deleteWaiting += 4;

                        if (item.deleteWaiting >= 125) {
                            var deletePromise = productListItemsService.delete(vm.listId, item.product.id).success(function () {
                                vm.products.remove(item);
                            });

                            vm.spinner.registerPromise(deletePromise);

                            $interval.cancel(item.deleteIntervalPromise);
                        }
                    }, 50, 0, true);
                }
            }

            vm.getMatches = function (text) {
                var defer = $q.defer();
                vm.spinner.registerPromise(productListService.productSearch(listId, text).success(function (r) {
                    if (r.length == 0) r.push({ isStub: true });
                    defer.resolve(r);
                }));
                //return [];
                return defer.promise;
            }

            vm.onSelect = function (item) {
                if (!item || item.isStub) {
                    vm.searchText = null;
                    vm.selectedItem = null;
                    return;
                }

                vm.spinner.registerPromise(productListItemsService.add(listId, item.id).success(function() {
                    vm.searchText = null;
                    vm.selectedItem = null;
                    reloadItems();
                }));
            }

            vm.createProduct = function(name) {
                vm.spinner.registerPromise(productListItemsService.addNewProduct(listId, name).success(function () {
                    vm.searchText = null;
                    vm.selectedItem = null;
                    reloadItems();
                }));
            }

            vm.restore = function (item) {
                console.log(item);
                vm.spinner.registerPromise(productListItemsService.restore(listId, item.id).success(function () {
                    //vm.completed.remove(item);
                    //vm.products.push({ product: item.product });
                    reloadItems();
                }));
            }

            var refreshInterval;

            function cancelAllIntervals() {
                if (refreshInterval) $interval.cancel(refreshInterval);
                vm.products.forEach(function(i) {
                    if (i.deleteIntervalPromise) $interval.cancel(i.deleteIntervalPromise);
                });
            }

            $scope.$on('$destroy', function() {
                cancelAllIntervals();
            });

            function init() {
                reloadItems().then(function () {
                    refreshInterval = $interval(function () {
                        if (!vm.spinner.show() && !item.removing && !item.compliting) {
                            vm.spinner.registerPromise(reloadItems());
                        }
                           
                    },5000);
                });
            }

            function reloadItems() {
                var loadPromise = productListService.get(listId).success(function (list) {
                    common.$rootScope.title = list.name;
                    vm.products = list.items;
                    if (list.suggestions) {
                        vm.products = vm.products.concat(list.suggestions.map(function(x) { return { product: x }; }));
                    }

                    vm.completed = list.completed;
                    //vm.completed = vm.products.slice();
                });

                return loadPromise;
            }



            init();
        }
    ]);
})(app);

