﻿(function (app) {
    app.controller('LoginController', [
        '$scope', '$state','common', 'authService',
        function ($scope, $state,common, authService) {
            var vm = this;

            vm.password = "";
            vm.email = "";
            vm.login = function() {
                var model = {
                    email: vm.email,
                    password: vm.password
                }

                authService.login(model).success(function() {
                    $state.go("lists.list");
                });
            }

            common.$rootScope.title = "Вход";
        }
    ]);
})(app);

