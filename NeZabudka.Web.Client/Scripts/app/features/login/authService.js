﻿(function () {
    "use strict";
    var api = angular.module("api");

    api.service("authService", [
        "apiServiceBase", function (apiServiceBase) {
            function getUrlBase() {
                return "/api/login";
            }

//            this.list = function() {
//                return apiServiceBase.get(getUrlBase());
//            }
//
//            this.get = function (id) {
//                return apiServiceBase.get(getUrlBase() + '/' + id);
//            }

            this.login = function (model) {
                return apiServiceBase.post(getUrlBase(), model);
            }

                     this.logout = function () {
                return apiServiceBase.delete(getUrlBase());
            }

            this.register = function (model) {
                return apiServiceBase.post(getUrlBase() + '/register', model);
            }
        }
    ]);
})();