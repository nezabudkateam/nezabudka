﻿(function(app) {
    app.config(function($stateProvider, $urlRouterProvider) {

        //
        // Now set up the states
        $stateProvider
            .state('auth', {
                parent: 'app',
                url: "/auth",
                template: '<ui-view/>'
            })
            .state('login', {
                parent: 'auth',
                url: "/login",
                templateUrl: 'scripts/app/features/login/_login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            });
    });
})(app);