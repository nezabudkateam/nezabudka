﻿(function () {
    "use strict";
    var api = angular.module("api");

    api.service("productListService", [
        "apiServiceBase", function (apiServiceBase) {
            function getUrlBase() {
                return "/api/product-lists";
            }

            this.list = function() {
                return apiServiceBase.get(getUrlBase());
            }

            this.get = function (id) {
                return apiServiceBase.get(getUrlBase() + '/' + id);
            }

            this.create = function () {
                return apiServiceBase.post(getUrlBase());
            }

            this.delete = function (id) {
                return apiServiceBase.delete(getUrlBase() + "/" + id);
            }

            this.addUser = function(listId, userId) {
                return apiServiceBase.put(getUrlBase() + '/' + listId + '/users/' + userId);
            }

            this.removeUser = function (listId, userId) {
                return apiServiceBase.delete(getUrlBase() + '/' + listId + '/users/' + userId);
            }

            this.productSearch = function (listId, term) {
                return apiServiceBase.get(getUrlBase() + "/" +listId+"/search?keyword="+ term);
            }
        }
    ]);
})();