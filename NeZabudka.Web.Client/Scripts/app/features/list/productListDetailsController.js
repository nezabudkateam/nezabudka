﻿(function (app) {
    app.controller('ProductListDetailsController', [
        '$scope', '$state', 'common','productListService','userService',
        function ($scope, $state, common,productListService, userService) {
            var vm = this;
            var $q = common.$q;
            var listId = $state.params.listId;
            vm.model = {};

            vm.save = function () {
                console.log(vm.model);
                //productListService.save
            }

            vm.getMatches = function(term) {
                var defer = $q.defer();

                userService.search(term).success(function(r) {
                    if (r.length > 0) {
                        defer.resolve(r);
                    } else {
                        defer.resolve({ isStub: true });
                    }
                });

                return defer.promise;
            }

            vm.onSelect = function(item) {
                if (!item) return;
                if (!item.isStub) {
                    vm.addUser(item);
                }

                vm.searchText = null;
                vm.selectedItem = null;
            }

            vm.addUser = function (userModel) {
                console.log(userModel);
                if (!vm.model.users.find(function (x) { return x.id == userModel.id })) {
                    productListService.addUser(listId, userModel.id).success(function () {
                        vm.model.users.push(userModel);
                    });
                }
            }

            vm.removeUser = function (userModel) {
                productListService.removeUser(listId, userModel.id).success(function () {
                    vm.model.users.remove(userModel);
                });
            }

            function init() {
                productListService.get(listId).success(function (resp) {
                    common.$rootScope.title = resp.name;
                    vm.model = resp;
                });
            }

            init();
        }
    ]);
})(app);

