﻿(function (app) {
    app.controller('ListController', [
        '$scope', '$state', 'common','productListService',
        function ($scope, $state,common, productListService) {
            var vm = this;

            vm.showDelete = false;

            vm.toProducts = function (item) {
                $state.go('products.list', { listId: item.id });
            }

            vm.lists = [];

            vm.isEmpty = function() {
                return !vm.lists || vm.lists.length == 0;
            }

            vm.openMenu = function ($mdOpenMenu, ev) {
                //originatorEv = ev;
                $mdOpenMenu(ev);
            };

            vm.details = function(item) {
                $state.go("lists.details", { listId: item.id });
            }


            vm.delete = function (item) {
                productListService.delete(item.id).success(function() {
                    reloadList();
                });
            }

            vm.create = function (item) {
                productListService.create().success(function(r) {
                    $state.go("products.list", { listId: r.id });
                });
            }

            function init() {
                reloadList();
            }

            function reloadList() {
                productListService.list().success(function (resp) {
                    resp.forEach(ensureDescription);
                    vm.lists = resp;
                });
            }

            function ensureDescription(productList) {
                productList.itemsList = productList.items.slice(0, 4).map(function (i) { return i.product.name; }).join(', ');       
            }

            init();

            common.$rootScope.title = "Списки покупок";
        }
    ]);
})(app);

