﻿(function(app) {
    app.config(function($stateProvider, $urlRouterProvider) {

        //
        // Now set up the states
        $stateProvider
            .state('lists', {
                parent:"app",
                abstract: true,
                url: "/list",
                template:"<div ui-view flex layout='column'></div>"
            })
            .state('lists.list', {
                url: "",
                templateUrl: "/scripts/app/features/list/_list.html",
                controller: "ListController",
                controllerAs: "vm"
            }).state('lists.details', {
                url: "/:listId/details",
                templateUrl: "/scripts/app/features/list/_details.html",
                controller: "ProductListDetailsController",
                controllerAs: "vm"
            });
    });
})(app);