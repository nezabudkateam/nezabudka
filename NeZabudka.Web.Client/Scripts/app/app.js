﻿app = angular.module('app', ['ngMaterial','ui.router','api']);

app.config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider
                .iconSet('action', '/content/fonts/action-icons.svg', 24)
                .iconSet('alert', '/content/fonts/alert-icons.svg', 24)
                .iconSet('av', '/content/fonts/av-icons.svg', 24)
                .iconSet('communication', '/content/fonts/communication-icons.svg', 24)
                .iconSet('content', '/content/fonts/content-icons.svg')
                .iconSet('device', '/content/fonts/device-icons.svg', 24)
                .iconSet('editor', '/content/fonts/editor-icons.svg', 24)
                .iconSet('file', '/content/fonts/file-icons.svg', 24)
                .iconSet('hardware', '/content/fonts/hardware-icons.svg', 24)
                .iconSet('icons', '/content/fonts/icons-icons.svg', 24)
                .iconSet('image', '/content/fonts/image-icons.svg', 24)
                .iconSet('maps', '/content/fonts/maps-icons.svg', 24)
                .iconSet('navigation', '/content/fonts/navigation-icons.svg', 24)
                .iconSet('notification', '/content/fonts/notification-icons.svg', 24)
                .iconSet('social', '/content/fonts/social-icons.svg', 24)
                .iconSet('toggle', '/content/fonts/toggle-icons.svg', 24)
}]);

app.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/app/list");
    $stateProvider
        .state('app', {
            abstract: true,
            url: "/app",
            views: {
                "content": {
                    template: "<div ui-view flex layout='column'></div>"
                },
                "header": {
                    templateUrl: 'scripts/app/layout/_header.html',
//                        controller: 'siteManageController',
//                        controllerAs: 'vm'
                },
                "footer": {
                    template: ''
                }

            }
        });

});