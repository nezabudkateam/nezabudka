﻿(function() {
    var app = angular.module("app");

    app.factory('spinnerService', [
        "$window", "$q", function($window, $q) {
            function SpinnerService() {
                var self = this;
                var count = 0;

                self.show = function() {
                    return count > 0;
                }

                self.hide = function() {
                    return !self.show();
                }

                self.enter = function() {
                    count++;
                }

                self.leave = function() {
                    count--;
                }

                self.registerPromise = function(promise) {
                    if (!promise) return;

                    self.enter();
                    $q.when(promise).finally(function() {
                        self.leave();
                    });
                }

                self.registerPromises = function(promises) {
                    if (!promises || !angular.isArray(promises)) return;

                    for (var i = 0; i < promises.length; ++i) {
                        self.registerPromise(promises[i]);
                    }
                }
            }

            return SpinnerService;
        }
    ]);
})();
