(function () {
    'use strict';

    // Define the common module 
    // Contains services:
    //  - common
    //  - logger
    //  - spinner
    var commonModule = angular.module('common', []);

    commonModule.factory('common',
        ['$q', '$rootScope', '$timeout','$interval', common]);

    function common($q, $rootScope, $timeout,$interval) {
        return {
            $q: $q,
            $timeout: $timeout,
            $interval: $interval,
            $rootScope: $rootScope
        }
    }
})();