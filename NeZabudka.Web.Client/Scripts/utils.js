﻿if (!Array.prototype.remove) {
    Array.prototype.remove = function(obj) {
        var idx = this.indexOf(obj);
        if (idx >= 0) {
            this.splice(idx, 1);
        }
    }
}