﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Routing;
using StructureMap;
using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

namespace NeZabudka.Web.Client.Infrastructure.IoC
{
    public class StructureMapControllerFactory : DefaultControllerFactory {
        private readonly IContainer _kernel;

        public StructureMapControllerFactory(IContainer kernel) {
            _kernel = kernel;
        }

        public override void ReleaseController(IController controller) {
            //do nothing
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType) {
            if (controllerType == null) {
                throw new HttpException(404, $"The controller for path '{requestContext.HttpContext.Request.Path}' could not be found.");
            }
            return (IController)_kernel.GetInstance(controllerType);
        }
    }

	public class StructureMapHttpControllerActivator : IHttpControllerActivator
	{
		private readonly IContainer container;

		public StructureMapHttpControllerActivator(IContainer container)
		{
			this.container = container;
		}

		public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
		{
			return container.GetInstance(controllerType) as IHttpController;
		}
	}

	public class StructureMapDependencyResolver : StructureMapScope, IDependencyResolver {
        private readonly IContainer _container;

        public StructureMapDependencyResolver(IContainer container) : base(container) {
            this._container = container;
        }

        public IDependencyScope BeginScope() {
            var childContainer = this._container.GetNestedContainer();
            return new StructureMapScope(childContainer);
        }
    }

    public class StructureMapScope : IDependencyScope {
        private readonly IContainer _container;

        public StructureMapScope(IContainer container) {
            if (container == null) {
                throw new ArgumentNullException(nameof(container));
            }
            this._container = container;
        }

        public object GetService(Type serviceType) {
            if (serviceType == null) {
                return null;
            }

            if (serviceType.IsAbstract || serviceType.IsInterface) {
                return this._container.TryGetInstance(serviceType);
            }

            return this._container.GetInstance(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType) {
            return this._container.GetAllInstances(serviceType).Cast<object>();
        }

        public void Dispose() {
            this._container.Dispose();
        }
    }
}