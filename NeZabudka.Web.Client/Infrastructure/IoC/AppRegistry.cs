﻿using System.Configuration;
using MongoDB.Driver;
using NeZabudka.Configuration;
using NeZabudka.Data;
using NeZabudka.Services;
using NeZabudka.Web.Client.Infrastructure.Configuration;
using StructureMap;

namespace NeZabudka.Web.Client.Infrastructure.IoC
{
	public class AppRegistry : Registry
	{
		public AppRegistry()
		{
			var connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
			var dbFactory = new DatabaseFactory(connectionString);

			For<IMongoDatabase>().Use(x => dbFactory.GetDatabase()).Singleton();

			Scan(x =>
			{
				x.AssemblyContainingType<DatabaseFactory>();
				x.AddAllTypesOf<IMongoRepository>();
				x.WithDefaultConventions();
			});

			Scan(x =>
			{
				x.AssemblyContainingType<IService>();
				x.AddAllTypesOf<IService>();
				x.WithDefaultConventions();
			});

			For<IApplicationSettings>().Use<ApplicationSettings>().Singleton();
			For<IProductSuggestionProvider>().Use<ProductSuggestionProvider>().Singleton();
		}
	}
}