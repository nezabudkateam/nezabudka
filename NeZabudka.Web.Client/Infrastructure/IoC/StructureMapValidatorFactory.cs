﻿using System;
using FluentValidation;
using StructureMap;

namespace NeZabudka.Web.Client.Infrastructure.IoC
{
    public class StructureMapValidatorFactory: IValidatorFactory
    {
        private readonly IContainer _container;

        public StructureMapValidatorFactory(IContainer container)
        {
            _container = container;
        }

        public IValidator<T> GetValidator<T>()
        {
            return (IValidator<T>)_container.GetInstance(typeof (T));
        }

        public IValidator GetValidator(Type type)
        {
            return (IValidator)_container.GetInstance(type);
        }
    }
}