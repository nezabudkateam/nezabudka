﻿using System.Configuration;
using NeZabudka.Configuration;

namespace NeZabudka.Web.Client.Infrastructure.Configuration
{
	public class ApplicationSettings : IApplicationSettings
	{
		public string ProductSuggestionProviderUrl => ConfigurationManager.AppSettings["product-suggestion-provider-url"];
		public bool DemoMode => ConfigurationManager.AppSettings["demo-mode"] == "true";
	}
}