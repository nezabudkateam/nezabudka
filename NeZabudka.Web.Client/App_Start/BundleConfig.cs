﻿using System.Web.Optimization;

namespace NeZabudka.Web.Client {
    public class BundleConfig {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/app")
                .Include("~/scripts/app/app.js")
                .IncludeDirectory("~/scripts/app/common", "*.js", true)
                .IncludeDirectory("~/scripts/app/services", "*.js", true)
                .IncludeDirectory("~/scripts/app/features", "*.js", true));
        }
    }
}
