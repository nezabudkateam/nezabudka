﻿using System.Web.Mvc;
using System.Web.Routing;
using NeZabudka.Web.Client.Controllers;

namespace NeZabudka.Web.Client {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.Map<HomeController>("home.index", "", nameof(HomeController.Index));
        }
    }

    public static class RouteCollectionExtension {
        public static void Map<T>(this RouteCollection routes, string key, string url, string action, object defaults = null, object constraints = null, string httpVerb = null) where T : Controller {
            var rd = new RouteValueDictionary(defaults);
            var type = typeof(T);
            rd["controller"] = type.Name.Replace("Controller", string.Empty);
            rd["action"] = action;
            if (!string.IsNullOrWhiteSpace(httpVerb)) {
                var crd = new RouteValueDictionary(constraints) { ["httpMethod"] = new HttpMethodConstraint(httpVerb) };
                constraints = crd;
            }

            routes.MapRoute(key, url, rd, constraints);
        }
    }
}
