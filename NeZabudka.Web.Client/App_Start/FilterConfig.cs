﻿using System.Web.Mvc;
using StructureMap;

namespace NeZabudka.Web.Client
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters, IContainer container)
		{
			filters.Add(new HandleErrorAttribute());
		}
	}
}
