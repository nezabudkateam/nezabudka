﻿using NeZabudka.Domain.Products;

namespace NeZabudka.Web.Client.Models
{
	public class ProductModel
	{
		public string Id
		{ get; set; }

		public string Name
		{ get; set; }

		public static ProductModel MapFrom(Product product)
		{
			return new ProductModel
			{
				Id = product.Id.ToString(),
				Name = product.Name
			};
		}
	}
}