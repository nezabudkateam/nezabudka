﻿using System;

namespace NeZabudka.Web.Client.Models
{
	public class ProductListItemModel
	{
		public ProductModel Product
		{ get; set; }

		public string CreatorId
		{ get; set; }

		public DateTime CreationTime
		{ get; set; }
	}

	public class ProductListPurchaseModel
	{
		public string Id
		{ get; set; }

		public ProductModel Product
		{ get; set; }

		public DateTime Date
		{ get; set; }

		public UserModel User
		{ get; set; }
	}

	public class ProductListModel
	{
		public string Id
		{ get; set; }

		public string Name
		{ get; set; }

		public string CreatorId
		{ get; set; }

		public UserModel[] Users
		{ get; set; }

		public ProductListItemModel[] Items
		{ get; set; }

		public ProductModel[] Suggestions
		{ get; set; }

		public ProductListPurchaseModel[] Completed
		{ get; set; }
	}
}