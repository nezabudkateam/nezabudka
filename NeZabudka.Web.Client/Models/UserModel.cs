﻿using NeZabudka.Domain.Identity;

namespace NeZabudka.Web.Client.Models
{
	public class UserModel
	{
		public string Id
		{ get; set; }

		public string Name
		{ get; set; }

		public string Email
		{ get; set; }

		public static UserModel MapFrom(User user)
		{
			return new UserModel
			{
				Id = user.Id.ToString(),
				Name = user.Name,
				Email = user.Email
			};
		}
	}
}