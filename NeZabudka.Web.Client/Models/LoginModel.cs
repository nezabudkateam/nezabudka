﻿namespace NeZabudka.Web.Client.Models
{
	public class LoginModel
	{
		public string Email
		{ get; set; }

		public string Password
		{ get; set; }
	}

	public class RegisterModel : LoginModel
	{
		public string Name
		{ get; set; }
	}
}