﻿using System.Web;
using StructureMap.Web.Pipeline;

namespace NeZabudka.Web.Client {
    public class MvcApplication : HttpApplication {
        protected void Application_Start() {
        }

        protected void Application_EndRequest() {
            HttpContextLifecycle.DisposeAndClearAll();
        }
    }
}
