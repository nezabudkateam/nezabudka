﻿namespace NeZabudka.Configuration
{
	public interface IApplicationSettings
	{
		string ProductSuggestionProviderUrl
		{ get; }

		bool DemoMode
		{ get; }
	}
}
