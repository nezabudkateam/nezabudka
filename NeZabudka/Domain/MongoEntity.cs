﻿using System;
using MongoDB.Bson;

namespace NeZabudka.Domain
{
    public class MongoEntity
    {
        public ObjectId Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public bool IsTransient => Id == ObjectId.Empty;
    }
}