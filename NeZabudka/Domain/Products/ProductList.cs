﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace NeZabudka.Domain.Products
{
	public class ProductListItem
	{
		public ObjectId ProductId
		{ get; protected set; }

		public ObjectId CreatorId
		{ get; protected set; }

		public DateTime CreationTime
		{ get; protected set; }

		public ProductListItem(ObjectId productId, ObjectId creatorId)
		{
			ProductId = productId;
			CreatorId = creatorId;
			CreationTime = DateTime.UtcNow;
        }
	}

	public class ProductList : NamedMongoEntity
	{
		public ObjectId CreatorId
		{ get; protected set; }

		public List<ObjectId> UserIds
		{ get; protected set; }

		public List<ProductListItem> Items
		{ get; protected set; }

		protected ProductList()
		{
		}

		public ProductList(ObjectId userId)
		{
			CreatorId = userId;
            UserIds = new List<ObjectId>();
			UserIds.Add(userId);
			Items = new List<ProductListItem>();
		}
	}
}
