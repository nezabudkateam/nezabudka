﻿using System;
using MongoDB.Bson;

namespace NeZabudka.Domain.Products
{
	public class ProductPurchaseInfo
	{
		public ObjectId ProductId
		{ get; protected set; }

		public DateTime Date
		{ get; set; }

		public ProductPurchaseInfo(ObjectId productId):this(productId, DateTime.UtcNow){}

	    public ProductPurchaseInfo(ObjectId productId, DateTime now) {
	        ProductId = productId;
	        Date = now;
	    }

    }

	public class ProductPurchase : MongoEntity
	{
		public ObjectId ListId
		{ get; protected set; }

		public ProductPurchaseInfo Info
		{ get; protected set; }

		public ObjectId UserId
		{ get; protected set; }

		protected ProductPurchase()
		{
		}

		public ProductPurchase(ObjectId listId, ObjectId productId, ObjectId userId)
		{
			ListId = listId;
            Info = new ProductPurchaseInfo(productId);
			UserId = userId;
		}
	}
}
