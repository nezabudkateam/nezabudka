﻿namespace NeZabudka.Domain
{
	public class NamedMongoEntity : MongoEntity
	{
		public string Name
		{ get; set; }
	}
}
