﻿namespace NeZabudka.Domain.Identity
{
	public class User : MongoEntity
	{
		public string Email
		{ get; set; }

		public string Password
		{ get; set; }

		public string Name
		{ get; set; }
	}
}