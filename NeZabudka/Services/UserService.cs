﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using NeZabudka.Configuration;
using NeZabudka.Data;
using NeZabudka.Domain.Identity;
using NeZabudka.Domain.Products;

namespace NeZabudka.Services
{
	public interface IUserService : IService
	{
		Task<User> GetUser(ObjectId id);
		Task<User> GetUserByEmail(string email);
		Task<List<User>> FindUsersByKeyword(string keyword, int count);
		Task<User> Register(string email, string password, string name);
	}

	public class UserService : IUserService
	{
		private readonly IUserRepository userRepository;
		private readonly IProductRepository productRepository;
		private readonly IProductListRepository productListRepository;
		private readonly IProductPurchaseRepository productPurchaseRepository;
		private readonly IApplicationSettings applicationSettings;

		public UserService(
			IUserRepository userRepository,
			IProductRepository productRepository,
			IProductListRepository productListRepository,
			IProductPurchaseRepository productPurchaseRepository,
			IApplicationSettings applicationSettings)
		{
			this.userRepository = userRepository;
			this.productRepository = productRepository;
			this.productListRepository = productListRepository;
			this.productPurchaseRepository = productPurchaseRepository;
			this.applicationSettings = applicationSettings;
		}

		public Task<User> GetUser(ObjectId id)
		{
			return userRepository.Get(id);
		}

		public Task<User> GetUserByEmail(string email)
		{
			return userRepository.GetByEmail(email);
		}

		public async Task<List<User>> FindUsersByKeyword(string keyword, int count)
		{
			var all = await userRepository.GetByKeyword(keyword);
			return all.Take(count).ToList();
		}

		/*private async Task GenerateRandomProductPurchases(IEnumerable<ObjectId> userIds)
		{
			var allProducts = await productRepository.List();
			var random = new Random();
			var selectedProductIds = new List<ObjectId>();
			for (int i = 0; i < 5; i++)
			{
				if (allProducts.Count == 0)
				{
					break;
				}
				var index = random.Next(allProducts.Count);
				selectedProductIds.Add(allProducts[index].Id);
				allProducts.RemoveAt(index);
			}
			foreach (var selectedProductId in selectedProductIds)
			{
				for (int i = random.Next(5) + 3; i >= 0; --i)
				{
					var maxTicks = DateTime.UtcNow.Ticks;
					var minTicks = maxTicks - 10000000;
					var purchase = new ProductPurchase(userIds, selectedProductId);
					purchase.Info.Date = new DateTime(minTicks + (maxTicks - minTicks) * random.Next(1000) / 1000);
					await productPurchaseRepository.Create(purchase);
				}
			}
		}*/

		public async Task<User> Register(string email, string password, string name)
		{
			var user = new User
			{
				Email = email,
				Password = password,
				Name = name
			};
			await userRepository.Create(user);
			await productListRepository.Create(new ProductList(user.Id) {  Name = "Новый список" });
			return user;
		}
	}
}
