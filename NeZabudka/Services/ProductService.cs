﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using NeZabudka.Data;
using NeZabudka.Domain.Products;

namespace NeZabudka.Services
{
	public interface IProductService : IService
	{
		Task<Product> GetProduct(ObjectId id);
		Task<List<Product>> GetProductsByKeyword(string keyword, int count, ObjectId userId);
		Task<Product> AddPrivateProduct(string name, ObjectId listId);
	}

	public class ProductService : IProductService
	{
		private readonly IProductRepository productRepository;
		private readonly IProductListRepository productListRepository;

		public ProductService(
			IProductRepository productRepository,
			IProductListRepository productListRepository)
		{
			this.productRepository = productRepository;
			this.productListRepository = productListRepository;
		}

		public Task<Product> GetProduct(ObjectId id)
		{
			return productRepository.Get(id);
		}

		public async Task<List<Product>> GetProductsByKeyword(string keyword, int count, ObjectId listId)
		{
			var all = await productRepository.GetByKeyword(keyword);
			return all.Where(p => !p.ListId.HasValue || (p.ListId == listId)).Take(count).ToList();
		}

		public async Task<Product> AddPrivateProduct(string name, ObjectId listId)
		{
			var product = new Product
			{
				Name = name,
				ListId = listId
			};
			await productRepository.Create(product);
			return product;
		}
	}
}
