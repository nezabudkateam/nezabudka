﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using NeZabudka.Data;
using NeZabudka.Domain.Products;

namespace NeZabudka.Services
{
	public interface IProductListService : IService
	{
		Task<ProductList> GetList(ObjectId id);
		Task<List<ProductList>> GetByUser(ObjectId userId);
		Task<ProductList> Create(ObjectId userId);
		Task Rename(ObjectId id, string newName);
		Task AddUser(ObjectId id, ObjectId userId);
		Task DeleteUser(ObjectId id, ObjectId userId);
		Task AddItem(ObjectId id, ObjectId productId, ObjectId userId);
		Task DeleteItem(ObjectId id, ObjectId productId, bool completed, ObjectId userId);
		Task<List<ProductPurchase>> GetPurchases(ObjectId id);
		Task RestorePurchase(ObjectId id, ObjectId purchaseId);
		Task Delete(ObjectId id);
	}

	public class ProductListService : IProductListService
	{
		private readonly IProductListRepository productListRepository;
		private readonly IProductPurchaseRepository productPurchaseRepository;

		public ProductListService(
			IProductListRepository productListRepository,
			IProductPurchaseRepository productPurchaseRepository)
		{
			this.productListRepository = productListRepository;
			this.productPurchaseRepository = productPurchaseRepository;
		}

		public Task<ProductList> GetList(ObjectId id)
		{
			return productListRepository.Get(id);
		}

		public Task<List<ProductList>> GetByUser(ObjectId userId)
		{
			return productListRepository.GetByUser(userId);
		}

		public async Task<ProductList> Create(ObjectId userId)
		{
			var lists = await productListRepository.GetByUser(userId);
			var newList = new ProductList(userId);
			var index = 0;
			do
			{
				++index;
				newList.Name = "Новый список " + index.ToString();
			}
			while (lists.Any(list => list.Name == newList.Name));
			await productListRepository.Create(newList);
			return newList;
		}

		public async Task Rename(ObjectId id, string newName)
		{
			var list = await productListRepository.Get(id);
			if (list.Name != newName)
			{
				list.Name = newName;
				await productListRepository.Update(list);
			}
		}

		public async Task AddUser(ObjectId id, ObjectId userId)
		{
			var list = await productListRepository.Get(id);
			if (!list.UserIds.Contains(userId))
			{
				list.UserIds.Add(userId);
				await productListRepository.Update(list);
			}
		}

		public async Task DeleteUser(ObjectId id, ObjectId userId)
		{
			var list = await productListRepository.Get(id);
			if ((list.CreatorId != userId) && list.UserIds.Remove(userId))
			{
				await productListRepository.Update(list);
			}
		}

		public Task Delete(ObjectId id)
		{
			return productListRepository.Delete(id);
		}

		public async Task AddItem(ObjectId id, ObjectId productId, ObjectId userId)
		{
			var list = await productListRepository.Get(id);
			var item = list.Items.FirstOrDefault(i => i.ProductId == productId);
			if (item == null)
			{
				item = new ProductListItem(productId, userId);
				list.Items.Add(item);
				await productListRepository.Update(list);
			}
		}

		public Task<List<ProductPurchase>> GetPurchases(ObjectId id)
		{
			return productPurchaseRepository.GetByList(id);
		}

		public async Task RestorePurchase(ObjectId id, ObjectId purchaseId)
		{
			var purchase = await productPurchaseRepository.Get(purchaseId);
			await AddItem(id, purchase.Info.ProductId, purchase.UserId);
			await productPurchaseRepository.Delete(purchaseId);
		}

		public async Task DeleteItem(ObjectId id, ObjectId productId, bool completed, ObjectId userId)
		{
			var list = await productListRepository.Get(id);
			if (list != null)
			{
				var item = list.Items.FirstOrDefault(i => i.ProductId == productId);
				if (item != null)
				{
					list.Items.Remove(item);
					if (completed)
					{
						await productPurchaseRepository.Create(new ProductPurchase(id, productId, userId));
					}
					await productListRepository.Update(list);
				}
				else
				{
					if (completed)
					{
						await productPurchaseRepository.Create(new ProductPurchase(id, productId, userId));
					}
				}
			}
		}
	}
}
