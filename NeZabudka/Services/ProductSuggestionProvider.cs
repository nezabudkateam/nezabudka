﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using Newtonsoft.Json;
using NeZabudka.Configuration;
using NeZabudka.Domain.Products;

namespace NeZabudka.Services
{
	public class ProductSuggestion
	{
		[JsonProperty("key")]
		public string ProductId
		{ get; set; }

		[JsonProperty("value")]
		public double Weight
		{ get; set; }
	}

	public interface IProductSuggestionProvider
	{
		Task<List<ProductSuggestion>> Suggest(IEnumerable<ProductPurchaseInfo> purchases, DateTime now);
	}

	public class ProductSuggestionProvider : IProductSuggestionProvider
	{
		private static readonly string[] AvailableAlgorithms = new string[] { "random_forest", "regressor", "simple_predictor" };
		private readonly IApplicationSettings applicationSettings;
		private readonly HttpClient client = new HttpClient();

		public ProductSuggestionProvider(IApplicationSettings applicationSettings)
		{
			this.applicationSettings = applicationSettings;
		}

		public async Task<List<ProductSuggestion>> Suggest(IEnumerable<ProductPurchaseInfo> purchases, DateTime now)
		{
			// {'data': {'item 1': ['date 1', 'date 2'], ...}, 'algo': 'regressor', 'current_date': 'some date'}
			var data = new Dictionary<string, HashSet<string>>();
			foreach (var group in purchases.GroupBy(ppi => ppi.ProductId))
			{
				data.Add(group.Key.ToString(), new HashSet<string>(group.Select(ppi => ppi.Date.ToString("O"))));
			}
		    var dataText = JsonConvert.SerializeObject(data);
            foreach (var algorithm in AvailableAlgorithms) {

                var formContent = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("data", dataText),
                    new KeyValuePair<string, string>("algo", algorithm),
                    new KeyValuePair<string, string>("current_date", now.ToString("O"))
                });

                try
				{
					var url = applicationSettings.ProductSuggestionProviderUrl;
					var response = await client.PostAsync(url, formContent);
					var result = await response.Content.ReadAsStringAsync();
					return JsonConvert.DeserializeObject<List<ProductSuggestion>>(result);
				}
				catch (Exception)
				{
				}
			}
			throw new ApplicationException("Не удалось получить список предложений.");
		}
	}
}
