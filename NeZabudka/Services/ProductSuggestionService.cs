﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using NeZabudka.Data;
using NeZabudka.Domain.Products;

namespace NeZabudka.Services
{
	public interface IProductSuggestionService : IService
	{
		Task<List<ProductSuggestion>> GetSuggestions(ProductList list);
	}

	public class ProductSuggestionService : IProductSuggestionService
	{
		private readonly IProductPurchaseRepository purchaseRepository;
		private readonly IProductSuggestionProvider suggestionProvider;
		private readonly MemoryCache cache = new MemoryCache("ProductSuggestionServiceMemoryCache");

		public ProductSuggestionService(
			IProductPurchaseRepository purchaseRepository,
			IProductSuggestionProvider suggestionProvider)
		{
			this.purchaseRepository = purchaseRepository;
			this.suggestionProvider = suggestionProvider;
		}

		public async Task<List<ProductSuggestion>> GetSuggestions(ProductList list)
		{
			var cacheKey = list.Id.ToString();
			var cachedValue = MemoryCache.Default.Get(cacheKey);
			if (cachedValue != null)
			{
				return (List<ProductSuggestion>)cachedValue;
			}
			var productPurchases = await purchaseRepository.GetByList(list.Id);
			var suggestions = await suggestionProvider.Suggest(productPurchases.Select(p => p.Info), DateTime.UtcNow);
			var result = suggestions.Where(s => (s.Weight > 0.1) && list.Items.All(i => i.ProductId.ToString() != s.ProductId));
			var suggestionList = result.OrderBy(s => s.Weight).ToList();
			MemoryCache.Default.Add(cacheKey, suggestionList, DateTimeOffset.Now.AddMinutes(30.0d));
			return suggestionList;
		}
	}
}
