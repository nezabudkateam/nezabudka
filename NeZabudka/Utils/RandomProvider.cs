﻿using System;

namespace NeZabudka.Utils
{
    public static class RandomProvider
    {
        private static Random random = null;

        public static Random GetRandom()
        {
            if (random == null)
            {
                random = new Random(Guid.NewGuid().GetHashCode());
            }

            return random;
        }
    }
}