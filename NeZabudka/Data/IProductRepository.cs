﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NeZabudka.Domain.Products;

namespace NeZabudka.Data
{
	public interface IProductRepository : IMongoRepository<Product>
	{
		Task<List<Product>> GetByKeyword(string keyword);
    }
}