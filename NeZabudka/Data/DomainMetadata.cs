﻿using System;
using System.Collections.Generic;
using NeZabudka.Domain.Identity;
using NeZabudka.Domain.Products;

namespace NeZabudka.Data
{
    public static class DomainMetadata {
        private static readonly IReadOnlyDictionary<Type, string> _storage = new Dictionary<Type, string> {
            {typeof (User), "users"},
			{typeof (Product), "products"},
			{typeof (ProductList), "productLists"},
			{typeof (ProductPurchase), "productPurchases"}
		};

        public static string StorageNameFor<TEntity>() {
            var type = typeof(TEntity);
            return StorageNameFor(type);
        }

        public static string StorageNameFor(Type type) {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            if (_storage.ContainsKey(type))
                return _storage[type];

            var t = type;
            do {
                t = t.BaseType;
                if (_storage.ContainsKey(t))
                    return _storage[t];
            } while (t != typeof(object));

            throw new InvalidOperationException($"Type '{type}' has no registered storage metadata, Please register it in DomainMetadata");
        }
    }
}