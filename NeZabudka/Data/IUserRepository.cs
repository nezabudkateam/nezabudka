﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NeZabudka.Domain.Identity;

namespace NeZabudka.Data
{
	public interface IUserRepository : IMongoRepository<User>
	{
		Task<List<User>> GetByKeyword(string keyword);
		Task<User> GetByEmail(string email);
	}
}