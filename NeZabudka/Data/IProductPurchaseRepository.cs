﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using NeZabudka.Domain.Products;

namespace NeZabudka.Data
{
	public interface IProductPurchaseRepository : IMongoRepository<ProductPurchase>
	{
		Task<List<ProductPurchase>> GetByList(ObjectId listId);
    }
}