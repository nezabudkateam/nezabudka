﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using NeZabudka.Domain;

namespace NeZabudka.Data
{
	public interface IMongoRepository
	{
		Task Clear();
		Task<long> Count();
	}

	public interface IMongoRepository<TEntity> : IMongoRepository
		where TEntity : MongoEntity
	{
        Task<List<TEntity>> List();
		Task<TEntity> Get(ObjectId id);
		Task Create(TEntity entity);
		Task Update(TEntity entity);
		Task Delete(ObjectId id);
	}

	public static class MongoRepositoryExtensions
	{
		public static Task Save<TEntity>(this IMongoRepository<TEntity> repository, TEntity entity)
			 where TEntity : MongoEntity
		{
			return entity.IsTransient ? repository.Create(entity) : repository.Update(entity);
		}
	}
}