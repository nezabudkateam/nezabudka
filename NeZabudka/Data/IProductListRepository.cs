﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using NeZabudka.Domain.Products;

namespace NeZabudka.Data
{
	public interface IProductListRepository : IMongoRepository<ProductList>
	{
		Task<List<ProductList>> GetByUser(ObjectId userId);
    }
}