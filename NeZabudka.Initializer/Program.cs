﻿using System;
using System.Threading.Tasks;
using MongoDB.Bson;
using NeZabudka.Data;
using NeZabudka.Data.Repositories;
using NeZabudka.Domain.Identity;
using NeZabudka.Domain.Products;

namespace NeZabudka.Initializer
{
	internal static class Program {
	    private static ObjectId ListId = ObjectId.Parse("373123645c2b66189cfef079");

        private static ProductListRepository lists;
        private static ProductRepository products;
        private static UserRepository users;
        private static ProductPurchaseRepository purchases;

        public static void Main(string[] args)
		{
			DataLayerConfiguration.Configure();
			var databaseFactory = new DatabaseFactory("mongodb://localhost/NeZabudka?safe=true");
			var database = databaseFactory.GetDatabase();
             lists = new ProductListRepository(database);
			 products = new ProductRepository(database);
			 users = new UserRepository(database);
            purchases = new ProductPurchaseRepository(database);

            Console.Write("Initializing...");
            Task.Run(async () => {
                await AddProducts(products);
                await AddUsers(users, lists);
            }).Wait();

			Console.WriteLine(" Done.");
        }

		public static Task Add(this IProductRepository products, string name)
		{
			return products.Save(new Product { Name = name });
		}

		private static async Task AddProducts(IProductRepository products)
		{
			await products.Clear();
			await products.Add("Апельсины");
			await products.Add("Бананы");
			await products.Add("Вода минеральная");
			await products.Add("Говядина");
			await products.Add("Горох");
			await products.Add("Гречка");
			await products.Add("Грибы");
			await products.Add("Груши");
			await products.Add("Дрожжи");
			await products.Add("Зелень");
			await products.Add("Икра");
			await products.Add("Йогурт");
			await products.Add("Какао");
			await products.Add("Капуста");
			await products.Add("Картофель");
			await products.Add("Кетчуп");
			await products.Add("Кефир");
			await products.Add("Колбаса");
			await products.Add("Конфеты");
			await products.Add("Котлеты");
			await products.Add("Кофе");
			await products.Add("Крахмал");
			await products.Add("Кукуруза");
			await products.Add("Курица");
			await products.Add("Лимоны");
			await products.Add("Лук");
			await products.Add("Майонез");
			await products.Add("Макароны");
			await products.Add("Мандарины");
			await products.Add("Маргарин");
			await products.Add("Масло подсолнечное");
			await products.Add("Масло сливочное");
			await products.Add("Мед");
			await products.Add("Молоко");
			await products.Add("Морковь");
			await products.Add("Мороженое");
			await products.Add("Мука");
			await products.Add("Мясо");
			await products.Add("Овсянка");
			await products.Add("Огурцы");
			await products.Add("Орехи");
			await products.Add("Паштет");
			await products.Add("Перец");
			await products.Add("Печенья");
			await products.Add("Пицца");
			await products.Add("Помидоры");
			await products.Add("Приправы");
			await products.Add("Пряники");
			await products.Add("Рис");
			await products.Add("Рыба");
			await products.Add("Салат");
			await products.Add("Сахар");
			await products.Add("Свекла");
			await products.Add("Свинина");
			await products.Add("Сливки");
			await products.Add("Сметана");
			await products.Add("Сок");
			await products.Add("Соль");
			await products.Add("Сосиски");
			await products.Add("Суши");
			await products.Add("Сыр");
			await products.Add("Творог");
			await products.Add("Торт");
			await products.Add("Тушенка");
			await products.Add("Фасоль");
			await products.Add("Хлеб");
			await products.Add("Чай");
			await products.Add("Чеснок");
			await products.Add("Шоколад");
			await products.Add("Яблоки");
			await products.Add("Яйца");

			await products.Add("Пиво");
			await products.Add("Водка");
			await products.Add("Коньяк");
			await products.Add("Вино");
			await products.Add("Шампанское");
			await products.Add("Лимонад");
		}

		private static async Task AddUsers(IUserRepository users, IProductListRepository lists)
		{			
			await users.Clear();

		    var vitaliy = new User {
		        Id = ObjectId.Parse("573713665c2b66189cfef077"),
		        Email = "1",
		        Password = "11",
		        Name = "Виталий"
		    };

            var stas = new User {
                Id = ObjectId.Parse("573713665c2b66189cfef078"),
                Email = "2",
                Password = "22",
                Name = "Стас"
            };

            var diman = new User {
                Id = ObjectId.Parse("573713665c2b66189cfef079"),
                Email = "3",
                Password = "33",
                Name = "Диман"
            };

            await users.Create(vitaliy);
			await users.Create(stas);
			await users.Create(diman);

            var list = new ProductList(vitaliy.Id);
		    list.Id = ListId;
		    list.Name = "Наш список";
            list.UserIds.Add(stas.Id);
            list.UserIds.Add(diman.Id);

		    await lists.Clear();
		    await lists.Create(list);

		    await AddPurchases(ListId);
		}

	    public static async Task AddPurchases(ObjectId listId) {
	        var bread = (await products.GetByKeyword("хлеб"))[0];
	        var milk = (await products.GetByKeyword("молоко"))[0];
	        var cheese = (await products.GetByKeyword("сыр"))[0];

	        for (int i = 1; i <= 10; ++i) {
                var purchase = new ProductPurchase(ListId, bread.Id, ObjectId.Parse("573713665c2b66189cfef077"));
                purchase.Info.Date = DateTime.UtcNow.AddDays(-3 * i);
	            await purchases.Create(purchase);
	        }

            for (int i = 1; i <= 10; ++i) {
                var purchase = new ProductPurchase(ListId, milk.Id, ObjectId.Parse("573713665c2b66189cfef077"));
                purchase.Info.Date = DateTime.UtcNow.AddDays(-5 * i);
                await purchases.Create(purchase);
            }

            var random = new Random(Guid.NewGuid().GetHashCode());

            for (int i = 1; i <= 10; ++i) {
                var purchase = new ProductPurchase(ListId, cheese.Id, ObjectId.Parse("573713665c2b66189cfef077"));
                purchase.Info.Date = DateTime.UtcNow.AddDays(-7 * i);
                purchase.Info.Date = purchase.Info.Date.AddDays(random.Next(-1, 1));
                await purchases.Create(purchase);
            }
        }
	}
}
