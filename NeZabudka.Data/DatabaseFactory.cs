﻿using MongoDB.Driver;

namespace NeZabudka.Data
{
    public class DatabaseFactory
    {
        private readonly IMongoDatabase _db;

        public DatabaseFactory(string connectionString) {
            var url = new MongoUrl(connectionString);
            var client = new MongoClient(url);
            _db = client.GetDatabase(url.DatabaseName);
        }

        public IMongoDatabase GetDatabase() {
            return _db;
        }
    }
}