﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using NeZabudka.Domain.Products;

namespace NeZabudka.Data.Repositories
{
	public class ProductRepository : MongoRepository<Product>, IProductRepository
	{
		public ProductRepository(IMongoDatabase database)
			: base(database)
		{
		}

		public Task<List<Product>> GetByKeyword(string keyword)
		{
			keyword = (keyword ?? "").ToLowerInvariant();
			return Collection.Find(p => p.Name.ToLowerInvariant().StartsWith(keyword)).ToListAsync();
		}
	}
}