﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using NeZabudka.Domain.Identity;

namespace NeZabudka.Data.Repositories
{
	public class UserRepository : MongoRepository<User>, IUserRepository
	{
		public UserRepository(IMongoDatabase database) : base(database)
		{
		}

		public Task<List<User>> GetByKeyword(string keyword)
		{
			keyword = (keyword ?? "").ToLowerInvariant();
			return Collection.Find(user => user.Name.ToLowerInvariant().Contains(keyword) || user.Email.ToLowerInvariant().Contains(keyword)).ToListAsync();
		}

		public Task<User> GetByEmail(string email)
		{
			return Collection.Find(user => user.Email == email).FirstOrDefaultAsync();
		}
	}
}