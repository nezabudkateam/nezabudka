﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using NeZabudka.Domain.Products;

namespace NeZabudka.Data.Repositories
{
	public class ProductPurchaseRepository : MongoRepository<ProductPurchase>, IProductPurchaseRepository
	{
		public ProductPurchaseRepository(IMongoDatabase database)
			: base(database)
		{
		}

		public Task<List<ProductPurchase>> GetByList(ObjectId listId)
		{
			return Collection.Find(purchase => purchase.ListId == listId).ToListAsync();
		}
	}
}