﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using NeZabudka.Domain.Products;

namespace NeZabudka.Data.Repositories
{
	public class ProductListRepository : MongoRepository<ProductList>, IProductListRepository
	{
		public ProductListRepository(IMongoDatabase database)
			: base(database)
		{
		}

		public Task<List<ProductList>> GetByUser(ObjectId userId)
		{
			return Collection.Find(list => list.UserIds.Contains(userId)).ToListAsync();
		}
	}
}