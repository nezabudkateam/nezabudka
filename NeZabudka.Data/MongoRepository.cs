﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using NeZabudka.Domain;

namespace NeZabudka.Data
{
	public abstract class MongoRepository<TEntity> : IMongoRepository<TEntity>
		where TEntity : MongoEntity
	{
		protected IMongoCollection<TEntity> Collection
		{ get; private set; }

		protected MongoRepository(IMongoDatabase database)
			: this(database, DomainMetadata.StorageNameFor<TEntity>())
		{
		}

		protected MongoRepository(IMongoDatabase database, string collectionName)
		{
			Collection = database.GetCollection<TEntity>(collectionName);
		}

		protected Task<long> Count(Expression<Func<TEntity, bool>> query)
		{
			return Collection.CountAsync(query);
		}

		protected Task Delete(Expression<Func<TEntity, bool>> query)
		{
			return Collection.DeleteManyAsync(query);
		}

		protected Task InsertBatch(IEnumerable<TEntity> entities)
		{
			var entitiesArr = entities.ToArray();
			return Collection.InsertManyAsync(entitiesArr);
		}

		public Task Clear()
		{
			var emptyFilter = new BsonDocument();
			return Collection.DeleteManyAsync(emptyFilter);
		}

		public Task<long> Count()
		{
			return Count(e => true);
		}

		public Task<List<TEntity>> List()
		{
			return Collection.Find(x => true).ToListAsync();
		}

		public Task<TEntity> Get(ObjectId id)
		{
			return Collection.Find(x => x.Id == id).SingleOrDefaultAsync();
		}

		public Task Create(TEntity entity)
		{
			return Collection.InsertOneAsync(entity);
		}

		public Task Update(TEntity entity)
		{
			return Collection.FindOneAndReplaceAsync(x => x.Id == entity.Id, entity);
		}

		public Task Delete(ObjectId id)
		{
			return Collection.DeleteOneAsync(x => x.Id == id);
		}
	}

	/*public class MongoRepository<TEntity>: IMongoRepository<TEntity> where TEntity: MongoEntity
    {
        protected readonly IMongoDatabase Database;
        protected readonly string CollectionName;
        private readonly IMongoCollection<TEntity> _collection;

        public MongoRepository(IMongoDatabase database):this(database, DomainMetadata.StorageNameFor<TEntity>())
        {
        }

        public MongoRepository(IMongoDatabase database, string collectionName)
        {
            Database = database;
            CollectionName = collectionName;
            _collection = Database.GetCollection<TEntity>(CollectionName);
        }

        public IMongoCollection<TEntity> Collection => _collection;

        public IMongoQueryable<TEntity> Queryable => Collection.AsQueryable(); 

        public Task Delete(TEntity entity) {
            throw new NotImplementedException();
        }

        public IMongoQueryable<TEntity> AsQueryable() {
            return _collection.AsQueryable();
        }

        public Task Delete(ObjectId id) {
            throw new NotImplementedException();
        }

        public async Task<TEntity> Get(ObjectId id)
        {
            return await _collection.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<TEntity> Save(TEntity entity)
        {
            ApplyAudit(entity);
            if (entity.IsTransient)
            {
                await _collection.InsertOneAsync(entity);
            }
            else
            {
                await Collection.FindOneAndReplaceAsync(x => x.Id == entity.Id, entity);
            }

            return entity;
        }

        private void ApplyAudit(TEntity entity)
        {
            if (entity.IsTransient)
            {
                entity.CreatedOn = DateTime.UtcNow;
            }

            entity.ModifiedOn = DateTime.UtcNow;
        }
    }*/
}