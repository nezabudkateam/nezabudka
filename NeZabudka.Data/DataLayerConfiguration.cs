﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using NeZabudka.Domain.Identity;

namespace NeZabudka.Data {
    public static class DataLayerConfiguration {
        public static void Configure() {
            BsonSerializer.RegisterIdGenerator(typeof(ObjectId), new ObjectIdGenerator());

            BsonClassMap.RegisterClassMap<User>(cm => {
                cm.AutoMap();
            });

          /*  BsonClassMap.RegisterClassMap<UserCredentials>(cm => {
                cm.AutoMap();
            });


            BsonClassMap.RegisterClassMap<AuthentificationData>(cm => {
                cm.AutoMap();
            });*/
        }
    }
}