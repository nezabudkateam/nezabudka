﻿using System.Security.Principal;
using System.Web;
using Microsoft.Owin;
using Owin;

namespace NeZabudka.Authentification
{
	public static class AuthExtensions
	{
		public static void UseNeZabudkaAuthentication(this IAppBuilder appBuilder)
		{
			AuthConfiguration.Configure(appBuilder);
        }

		public static AuthData GetNeZabudkaAuthData(this IPrincipal pricipal)
		{
			return AuthManager.GetData(pricipal);
		}

		public static void LoginToNeZabudka(this IOwinContext context, AuthData authData, bool persistent)
		{
			AuthManager.Login(context, authData, persistent);
        }

		public static void LoginToNeZabudka(this HttpContext context, AuthData authData, bool persistent)
		{
			AuthManager.Login(context, authData, persistent);
		}

		public static void LogoutFromNeZabudka(this IOwinContext context)
		{
			AuthManager.Logout(context);
		}

		public static void LogoutFromNeZabudka(this HttpContext context)
		{
			AuthManager.Logout(context);
        }
	}
}
