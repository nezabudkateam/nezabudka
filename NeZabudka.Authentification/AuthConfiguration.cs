﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace NeZabudka.Authentification
{
	public static class AuthConfiguration
	{
		public static void Configure(IAppBuilder appBuilder)
		{
			appBuilder.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationType = AuthentificationTypes.User,
				AuthenticationMode = AuthenticationMode.Active,
				CookieName = AuthCookieNames.User,
				ExpireTimeSpan = TimeSpan.FromDays(3),
				SlidingExpiration = true
			});
		}
	}
}
