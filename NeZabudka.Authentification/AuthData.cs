﻿namespace NeZabudka.Authentification
{
	public class AuthData
	{
		public string Id
		{ get; set; }

		public string Email
		{ get; set; }

		public string Name
		{ get; set; }
	}
}
