﻿using System.Security.Claims;

namespace NeZabudka.Authentification
{
	public static class AuthentificationTypes
	{
		public const string User = "NzUser";

		public static readonly string[] All = new string[] { User };
	}

	public static class AuthCookieNames
	{
		public const string User = "nz";
	}

	public static class AuthClaimTypes
	{
		public const string Id = ClaimTypes.NameIdentifier;
		public const string Email = ClaimTypes.Email;
		public const string Name = ClaimTypes.Name;
	}
}
