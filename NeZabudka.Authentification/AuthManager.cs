﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Web;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace NeZabudka.Authentification
{
	public static class AuthManager
	{
		public static AuthData GetData(ClaimsIdentity claimsIdentity)
		{
			var id = ClaimTools.ReadString(claimsIdentity, AuthClaimTypes.Id);
			if (id != null)
			{
				return new AuthData
				{
					Id = id,
					Email = ClaimTools.ReadString(claimsIdentity, AuthClaimTypes.Email),
					Name = ClaimTools.ReadString(claimsIdentity, AuthClaimTypes.Name)
				};
			}
			return null;
		}

		public static List<Claim> GetClaims(AuthData authData)
		{
			var claims = new List<Claim>();
			ClaimTools.AddString(claims, AuthClaimTypes.Id, authData.Id);
			ClaimTools.AddString(claims, AuthClaimTypes.Email, authData.Email);
			ClaimTools.AddString(claims, AuthClaimTypes.Name, authData.Name);
			return claims;
		}

		public static AuthData GetData(IPrincipal pricipal)
		{
			var claimsPrincipal = pricipal as ClaimsPrincipal;
			if (claimsPrincipal != null)
			{
				var claimsIdentity = claimsPrincipal.Identity as ClaimsIdentity;
				if (claimsIdentity != null && claimsIdentity.IsAuthenticated)
				{
					return GetData(claimsIdentity);
				}
			}
			return null;
		}

		public static AuthData GetCurrentData()
		{
			return GetData(Thread.CurrentPrincipal);
		}

		public static void Login(IOwinContext context, AuthData authData, bool persistent)
		{
			var properties = new AuthenticationProperties { IsPersistent = persistent };
			var identity = new ClaimsIdentity(GetClaims(authData), AuthentificationTypes.User);
			context.Authentication.SignIn(properties, identity);
		}

		public static void Login(HttpContext context, AuthData authData, bool persistent)
		{
			Login(context.GetOwinContext(), authData, persistent);
		}

		public static void Login(AuthData authData, bool persistent)
		{
			Login(HttpContext.Current, authData, persistent);
		}

		public static void Logout(IOwinContext context)
		{
			context.Authentication.SignOut(AuthentificationTypes.All);
		}

		public static void Logout(HttpContext context)
		{
			Logout(context.GetOwinContext());
		}

		public static void Logout()
		{
			Logout(HttpContext.Current);
		}
	}
}