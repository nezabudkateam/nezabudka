﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace NeZabudka.Authentification
{
	internal static class ClaimTools
	{
		public const string TrueString = "1";

		public static string ReadString(ClaimsIdentity claimsIdentity, string name)
		{
			return claimsIdentity.FindFirst(name)?.Value;
		}

		public static List<string> ReadStrings(ClaimsIdentity claimsIdentity, string name)
		{
			return claimsIdentity.FindAll(name).Select(c => c.Value).ToList();
		}

		public static Guid? ReadGuid(ClaimsIdentity claimsIdentity, string name)
		{
			Guid guid;
			var s = ReadString(claimsIdentity, name);
			return Guid.TryParse(s, out guid) ? (Guid?)guid : null;
		}

		public static bool ReadBoolean(ClaimsIdentity claimsIdentity, string name)
		{
			return ReadString(claimsIdentity, name) == TrueString;
		}

		public static void AddString(ICollection<Claim> claims, string name, string value)
		{
			if (!ReferenceEquals(value, null))
			{
				claims.Add(new Claim(name, value));
			}
		}

		public static void AddStrings(ICollection<Claim> claims, string name, IEnumerable<string> values)
		{
			if (values != null)
			{
				foreach (var value in values)
				{
					AddString(claims, name, value);
				}
			}
		}

		public static void AddGuid(ICollection<Claim> claims, string name, Guid value)
		{
			AddString(claims, name, value.ToString());
		}

		public static void AddBoolean(ICollection<Claim> claims, string name, bool value)
		{
			if (value)
			{
				AddString(claims, name, TrueString);
			}
		}
	}
}
